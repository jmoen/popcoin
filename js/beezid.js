var playerBetCount = 0;
var playerTotalWinnings = 0;
var playerTotalWithdrawableWinnings = 0;
var playerTotalMinings = 0;
var playerBetData = {}
var timeFormat = 'MM/DD/YYYY HH:mm';
var currentPotSize = 0;

function newDate(days) {
  return moment().add(days, 'd').toDate();
}

function newDateString(days) {
  return moment().add(days, 'd').format(timeFormat);
}

function newTimestamp(days) {
  return moment().add(days, 'd').unix();
}

function getDataPointForStartPoint(startPoint) {
  for(key in playerBetData){
    var thisDataPoint = playerBetData[key];
    if(thisDataPoint.betStartPointScaled == startPoint){
      return thisDataPoint;
    }
  }
  return null;
}

function scalePlayerBetData(betData, potSize) {
  for(key in betData){
    var thisDataPoint = betData[key];
    if(thisDataPoint.betStartPointRaw != undefined) {
      thisDataPoint.betStartPointScaled = (thisDataPoint.betStartPointRaw/potSize) * 100;
      thisDataPoint.betValueScaled = (thisDataPoint.betValueRaw/potSize) * 100;
    }
  }
  return betData;
}

App = {
  web3Provider: null,
  contracts: {},
  filter: null,
  potSize: 0,
  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    }

    web3 = new Web3(App.web3Provider);
    return App.initContract();
  },
  initContract: function() {
    $.getJSON('contracts/pop/pop.json', function(data) {
      var PopCoinArtifact = data;
      App.contracts.PopCoin = TruffleContract(PopCoinArtifact);
      if(App.web3Provider){
        // $("#game-stats").css("display", "block");
        $("#metamask-info").css("display", "none");
        App.contracts.PopCoin.setProvider(App.web3Provider);
      } else {
        console.log("metamask not found");
        // $("#game-stats").css("display", "none");
        $("#metamask-info").css("display", "block");
      }
      
      App.reloadGame();
    });
    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-bet', App.handleBet);
    $(document).on('click', '#withdrawEthButton', App.withdrawEth);
    $(document).on('click', '#withdrawPopButton', App.withdrawEth);
    $(document).on('click', "#redeemButton", App.redeemPop);
  },
  reloadGame: function(){
      App.loadBr();
      App.getPlayerBetCountAndLoadWinnings();
      App.loadBlocksNeeded();
      App.loadAmountRaked();
      App.loadMiningDifficulty();
      App.loadBlocksUntilGameEnd();
  },
  getPlayersBets: function(bets, playerBetCount) {
    var popcoinInstance;
    var currentGameNumber;
    App.contracts.PopCoin.deployed().then(function(instance){
      popcoinInstance = instance;
      return popcoinInstance.currentGameNumber.call();
    }).then(function(gameNumber){
      currentGameNumber = gameNumber.toNumber();
      return popcoinInstance.getBetRecord.call(playerBetCount);
    }).then(function(betRecord){
      var thisBetDataPoint = playerBetData[playerBetCount];
      if(!thisBetDataPoint) {
        thisBetDataPoint = {betNumber: playerBetCount}
        playerBetData[playerBetCount] = thisBetDataPoint;
      }

      thisBetDataPoint.betStartPointRaw = betRecord[0].toNumber();
      thisBetDataPoint.betValueRaw = betRecord[1].toNumber();

      if(betRecord[2].toNumber() == currentGameNumber){
        bets.push([betRecord[0].toNumber(), betRecord[1].toNumber()]);
      }
      playerBetCount -= 1;
      if(playerBetCount < 0){
        App.loadChart(bets)
      } else {
        App.getPlayersBets(bets, playerBetCount)
      }
    }).catch(function(e){
      console.log("error getting bets");
      console.log(e);
    })
  },
  startGame: function(betAmount){
    web3.eth.getAccounts(function(error, accounts){
      var account = accounts[0];
      App.contracts.PopCoin.deployed().then(function(instance){
          var value = web3.toWei(1, 'ether') * betAmount
          return instance.startGame({from: account, value: value});
      });
    });
  },
  updateRakePercent: function(percent) {
    web3.eth.getAccounts(function(error, accounts){
      var account = accounts[0];
      App.contracts.PopCoin.deployed().then(function(instance){
          return instance.updateAmountToTakeAsRake(percent);
      });
    });
  },
  increaseBonus: function(amount){
    web3.eth.getAccounts(function(error, accounts){
      var account = accounts[0];
      App.contracts.PopCoin.deployed().then(function(instance){
          var value = web3.toWei(1, 'ether') * amount
          return instance.addToBonusSeed({from: account, value: value});
      });
    });
  },
  loadChart: function(_playerBets) {
    var currentPot;
    var initialBet;
    var seedAmount;
    var popcoinInstance;
    console.log(App.contracts.PopCoin);
    App.contracts.PopCoin.deployed().then(function(instance){
      console.log("loaded instance");
      popcoinInstance = instance;
      return popcoinInstance.currentPot.call();
    }).then(function(potSize){
      currentPot = potSize.toNumber();
      App.potSize = currentPot;
      return popcoinInstance.initialSeed.call();
    }).then(function(initialSeed){
      seedAmount = initialSeed.toNumber();
      App.enableBetting(seedAmount != 0);
      return popcoinInstance.initialBankrollGrowthAmount.call();
    }).then(function(startingPotSize){
      initialBet = startingPotSize.toNumber();
      initialBet = initialBet/4294967296
      var adjustedPot = currentPot - seedAmount;
      var playerRanges = getPlayerRanges(_playerBets, adjustedPot);
      playerBetData = scalePlayerBetData(playerBetData, adjustedPot);

      var thisDataSet = createBets(playerRanges, currentPot, seedAmount, initialBet);
      function isPlayerStartPoint(index){
        for(var i=0; i<playerRanges.length; i++){
          var thisPlayerRange = playerRanges[i];
          if(thisPlayerRange[0] == index){
            return true;
          }
        }
        return false;
      }

      function indexContainedInPlayerRange(index){
        for(var i=0; i<playerRanges.length; i++){
          var thisPlayerRange = playerRanges[i];
          var thisStartPoint = thisPlayerRange[0];
          var thisEndPoint = thisPlayerRange[1];
          if(index >= thisStartPoint && index <= thisEndPoint){
            return true;
          }
        }
        return false;
      }

      var config = {
          type: 'line',
          data: {
            labels: [newDate(0), newDate(99)],
            datasets:[{
            label: "Dataset with point data",
            backgroundColor: '#fa3',
            fill: true,
            data: thisDataSet,
              }]
          },
          options: {
            responsive: true,
            elements: {
              point: {
                pointStyle: 'circle',
                borderWidth: 2,
                borderColor: 'rgba(153, 102, 255, 0.6)',
                radius: 0
              },
              line: {
                playerLineColor: 'rgba(54, 162, 235, 0.8)',
                defaultLineColor: 'rgba(40, 40, 40, 0.0)'
              },
              tooltips: {
                mode: 'y'
              }
            },
            legend: {
              display: true,
              position: 'top',
              labels: {
                generateLabels: function(){
                  return [{text: "Your Bets", fillStyle: "orange"}, {text: "Your Winnings", fillStyle: 'rgba(54, 162, 235, 0.6)'}, {text: "Other Players", fillStyle: 'rgba(54, 162, 235, 0.2)'}]
                },
              }
            },
            title: {
              display: false,
              text: "Heart of the Coin: The Game"
            },
            tooltips: {
              titleFontSize: 22,
              bodyFontSize: 18,
              displayColors: false,
              callbacks: {
                title: function(a, d) {
                  
                  var thisStartPoint = d.datasets[0].data[a[0].index].rawVal;
                  var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
                  return "Bet Size: " + (thisDataPoint.betValueRaw/web3.toWei(1, 'ether')).toFixed(3) + " ETH";
                },
                body: function(a, d){
                },
                label: function(a, d) {
                  var thisStartPoint = d.datasets[0].data[a.index].rawVal;
                  var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
                  return "Eth Winnings: " + (thisDataPoint.betWinnings/web3.toWei(1, 'ether')).toFixed(3);
                },
                afterBody: function(a, d) {
                  var thisStartPoint = d.datasets[0].data[a[0].index].rawVal;
                  var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
                  return "Pop Minings: " + (thisDataPoint.betMinings/web3.toWei(1, 'ether')).toFixed(3);
                }
              }
            },
            scales: {
              xAxes: [{
                display: true,
                type: "time",
                time: {
                  format: timeFormat,
                  // round: 'day'
                  tooltipFormat: 'll HH:mm'
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Eth Deposited'
                },
                ticks: {
                  callback: function(value, index, values){
                    return ((((index+1)/(values.length+1)) * (currentPotSize - seedAmount))/web3.toWei(1, 'ether')).toFixed(3);
                  }
                }
              }, ],
              yAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: 'Bet Multiplier'
                }
              }]
            },
            playerStartPoint: function(index){
              return isPlayerStartPoint(index);
            },
            pointData: function(index){
              if(isPlayerStartPoint(index)){
                return {borderWidth: 2, radius: 5}
              } else {
                return {borderWidth: 0, radius: 0}
              }
            },
            isPointInPlayerRange: function(index){
              return indexContainedInPlayerRange(index);
            },
            playerFillColor: 'rgba(54, 162, 235, 0.6)',
            defaultGameFillColor: 'rgba(54, 162, 235, 0.35)'
        }
      };
      var ctx = document.getElementById("myChart").getContext("2d");
      window.myLine = new Chart(ctx, config);
    }).catch(function(e){
      console.log("error loading chart info: ");
      console.log(e);
    });
  },
  loadBr: function() {
    var beezidInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      beezidInstance = instance;
      return beezidInstance.currentPot.call();
    }).then(function(br){
      currentPotSize = br.toNumber();
      $("#br").text((currentPotSize/web3.toWei(1, 'ether')).toFixed(3));
    });
  },
  enableBetting: function(enabled) {
      if(enabled){
        $("#timer").css("display", "none");
        $("#bet-button").removeAttr("disabled");
        $("#bet-button").css("opacity", "1.0")
      } else {
        $("#bet-button").attr("disabled", "disabled");
        $("#bet-button").css("opacity", "0.5")
        $("#timer").css("display", "block");
      }
  },
  loadBlocksUntilGameEnd: function() {
    App.contracts.PopCoin.deployed().then(function(instance){

      web3.eth.getBlockNumber(function(e, blockNumber){
        App.calcBlocksUntilEndOfGame(blockNumber);
      });
    });
  },
  calcBlocksUntilEndOfGame: function(blockNumber) {
    var popcoinIstance;
    var minBlocks;
    var lastBet;
    App.contracts.PopCoin.deployed().then(function(instance){
      popcoinInstance = instance;
      return popcoinInstance.minimumNumberOfBlocksToEndGame.call()
    }).then(function(minNumberOfBlocksToEndGame){
      minBlocks = minNumberOfBlocksToEndGame;
      return popcoinInstance.lastBetBlockNumber.call();
    }).then(function(lastBetBlockNumber){
      var blocksTillEnd = lastBetBlockNumber.toNumber() == 0 ? minBlocks.toNumber() : minBlocks.toNumber() - (blockNumber - lastBetBlockNumber.toNumber());
      blocksTillEnd = Math.max(0, blocksTillEnd);
      blocksTillEnd = Math.min(blocksTillEnd, minBlocks.toNumber());
      $("#blocksUntilGameEnd").text(blocksTillEnd);
    });
  },
  loadPlayerWinningsForBet: function(totalWinnings, betId) {
    var popcoinInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getWinningsForRecordId.call(betId, false, true)
    }).then(function(playerWinnings){

      var thisBetDataPoint = playerBetData[betId];
      if(!thisBetDataPoint) {
        thisBetDataPoint = {betNumber: betId}
        playerBetData[betId] = thisBetDataPoint
      }

      thisBetDataPoint.betWinnings = playerWinnings.toNumber();

      totalWinnings += playerWinnings.toNumber();
      betId -= 1;
      if(betId >= 0){
        App.loadPlayerWinningsForBet(totalWinnings, betId);
      } else {
        playerBetData = scalePlayerBetData(playerBetData, currentPotSize);

        $("#playerWinnings").text((totalWinnings/web3.toWei(1, 'ether')).toFixed(4));
      }
    });
  },
  loadPlayerMinings: function(totalMinings, betId){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.playerPopMining.call(betId, true);
    }).then(function(playerMinings){

      var thisBetDataPoint = playerBetData[betId];
      if(!thisBetDataPoint) {
        thisBetDataPoint = {betNumber: betId}
        playerBetData[betId] = thisBetDataPoint
      }

      thisBetDataPoint.betMinings = playerMinings.toNumber();

      totalMinings += playerMinings.toNumber();
      betId -= 1;

      if(betId >= 0){
        App.loadPlayerMinings(totalMinings, betId);
      } else {
        playerTotalMinings = totalMinings
        playerBetData = scalePlayerBetData(playerBetData, currentPotSize);
        var playerMiningsAdjust = (playerTotalMinings/(web3.toWei(1, 'ether'))).toFixed(3);
        $("#playerMinings").text(playerMiningsAdjust);
      }
    })
  },
  loadPlayerMiningsWithdrawable: function(totalMinings, betId) {
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.playerPopMining.call(betId, false);
    }).then(function(playerMinings){

      totalMinings += playerMinings.toNumber();
      betId -= 1;

      if(betId >= 0){
        App.loadPlayerMiningsWithdrawable(totalMinings, betId);
      } else {
        playerTotalMinings = totalMinings
        playerBetData = scalePlayerBetData(playerBetData, currentPotSize);
        var playerMiningsAdjust = (playerTotalMinings/(web3.toWei(1, 'ether'))).toFixed(3);
        $("#playerMiningsWithdrawable").text(playerMiningsAdjust);
      }
    })
  },
  loadMiningDifficulty: function(){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getCurrentMiningDifficulty.call();
    }).then(function(miningDifficulty){
      $("#currentDifficulty").text((miningDifficulty.toNumber()/1000000).toFixed(3));
    });
  },
  loadWithdrawableWinningsForBet: function(totalWinnings, betId) {
    var popcoinInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getWinningsForRecordId.call(betId, true, false);
    }).then(function(playerWinningsWithdrawable){
      totalWinnings += playerWinningsWithdrawable.toNumber();
      betId -= 1;
      if(betId >= 0){
        App.loadWithdrawableWinningsForBet(totalWinnings, betId);
      } else {
        App.finalizePlayerWinnings(totalWinnings);
      }
    });
  },
  finalizePlayerWinnings: function(betRecordWinnings){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getPlayerInternalWallet.call();
    }).then(function(playerInternalWalletWinnings){
      var playerTotalWinnings = playerInternalWalletWinnings.toNumber() + betRecordWinnings;
      $("#playerWinningsWithdrawable").text((playerTotalWinnings/web3.toWei(1, 'ether')).toFixed(3));
    });
  },
  withdrawEth: function() {
    var popcoinInstance;
    web3.eth.getAccounts(function(error, accounts){
      if(error){
        console.log(error);
      }
      var account = accounts[0];
      App.contracts.PopCoin.deployed().then(function(instance){
        popcoinInstance = instance;
        return popcoinInstance.withdraw(7, {gas: 432766});
      }).then(function(result){
        setTimeout(function(){
          App.reloadGame();
        }, 5000);
      }).catch(function(err){
        console.log("withdrawEth error: ");
        console.log(err);
      })
    });
  },
  loadBlocksNeeded: function(){
    var beezidInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.minimumNumberOfBlocksToEndGame.call();
    }).then(function(blocksNeeded){
      $("#blocksNeeded").text(blocksNeeded.toNumber());
    });
  },
  loadAmountRaked: function(){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.totalAmountRaked.call();
    }).then(function(totalRaked){
      $("#totalRaked").text((totalRaked.toNumber()/web3.toWei(1, 'ether')).toFixed(3))
    });
  },
  getPlayerBetCountAndLoadWinnings: function(){
    var popcoinInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      popcoinInstance = instance;
      return popcoinInstance.getMyBetRecordCount.call();
    }).then(function(betCount){
      playerBetCount = betCount.toNumber();
      playerBetCount -= 1;
      if(playerBetCount >= 0){
        App.getPlayersBets([], playerBetCount);
        App.loadPlayerWinningsForBet(0, playerBetCount);
        App.loadWithdrawableWinningsForBet(0, playerBetCount);
        App.loadPlayerMinings(0, playerBetCount);
        App.loadPlayerMiningsWithdrawable(0, playerBetCount);
      } else {
        App.finalizePlayerWinnings(0);
        App.loadChart([]);
      }
    }).catch(function(e){
      App.enableBetting(false);
    })
  },
  loadTotalMinings: function() {
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.totalSupply.call();
    }).then(function(gameMinings){
      var totalGameMinings = (gameMinings.toNumber()/(web3.toWei(1, 'ether'))).toFixed(3);

      $("#playerMiningsWithdrawable").text(totalGameMinings);
    });
  },
  redeemPop: function(){
    var popcoinInstance;
    web3.eth.getAccounts(function(error, accounts){
      if (error) {
        console.log(error);
      }
      var account = accounts[0];

      App.contracts.PopCoin.deployed().then(function(instance){
        popcoinInstance = instance;
        return popcoinInstance.balanceOf.call(account)
      }).then(function(popBalance){
        return popcoinInstance.redeemPop(popBalance.toString());
      }).then(function(result){
      }).catch(function(err){
        console.log(err);
      });
    });
  },
  handleBet: function() {
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
      var betAmountRaw = $("#bet-amount").val()
      var betAmount = Number(betAmountRaw)
      var account = accounts[0];
      var beezidInstance;
      App.contracts.PopCoin.deployed().then(function(instance){
        beezidInstance = instance;
        var value = web3.toWei(1, 'ether') * betAmount
        return beezidInstance.bet({from: account, value: value});
      }).then(function(result){
        setTimeout(function() {
          App.reloadGame();
        }, 5000);
      }).catch(function(err){
        console.log(err);
      });
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
