var pi = 3.14159265;
var _2pi = 6.2831853;
var _timeFormat = 'MM/DD/YYYY HH:mm';

function createDateString(days) {
	//return "" + days;
	var hours = 24 * days
	return moment().add(hours, 'h').format(_timeFormat);
}

function getEndPointSize(adjustmentRatio, startingBr, endingBr) {
	var brGrowthRatio = endingBr/startingBr;

	var growthRate = Math.log(brGrowthRatio);
	var windowAdjustment = brGrowthRatio/(adjustmentRatio ** growthRate);
	return windowAdjustment;
}

function integrate(x, a) {
	return (Math.sin(a * x)/a) + x
}

function calcDepositSums(players) {
	var deposits = [];
	for(var i=0; i<players.length; i++) {
		var player = players[i];
		if(i == 0){
			deposits.push(player)
		} else {
			var lastDeposit = deposits[i-1];
			deposits.append(lastDeposit + player);
		}
	}
	return deposits;
}

function preprocessPlayerBets(playerBets, potSize) {
	var processedRecords = [];

	for(var i=0; i<playerBets.length; i++){
		var thisBet = playerBets[i];
		var newEntry = [(thisBet[0]/potSize) * 100, (thisBet[1]/potSize) * 100];
		processedRecords.push(newEntry);
	}
	return processedRecords;
}

function getPlayerRanges(playerBets, potSize) {
	playerBets.reverse()
	function makePercent(bet) {
		return (bet/potSize) * 100;
	}
	var playerRanges = [];
	for(var i=0; i<playerBets.length; i++){
		var thisBet = playerBets[i];
		var startPoint = thisBet[0];
		var endPoint = startPoint + thisBet[1];
		playerRanges.push([makePercent(startPoint), makePercent(endPoint)]);
	}

	return playerRanges;
}

function createBets(playerRanges, potSize, seedAmount, firstBet) {

	var players = [];
	var depositSums = [];
	for(var i=1; i<=100; i++){
		players.push(1);
		depositSums.push(i);
	}

	var payouts = calculatePayouts(players, depositSums, 100, (potSize/(potSize - seedAmount)) * 100, (firstBet/potSize)*100);

	var data = [];
	for(var i=0; i<payouts.length; i++){
		var thisData = {x: createDateString(i), y:payouts[i], rawVal:i};
		data.push(thisData);
	}

	var playerPoints = [];
	var processedPoints = {};
	for(var i=0; i<playerRanges.length; i++){
		function createNewData(point){
			if(point > 100){
				point = 100.0
			}
			if(processedPoints[point]) {
				return null;
			}
			processedPoints[point] = true;
			//find 2 points that it is in between, check that it is actually between the points
			var startPointFloor = Math.floor(point);
			var startPointCeil = Math.ceil(point);
			if(startPointFloor == point || startPointCeil == point) {
				return null;
			}

			//calculate slope of those 2 points
			var slope = data[startPointFloor+1].y - data[startPointFloor].y;
			//get difference in run between startPoint and point we're adding
			var movement = point - startPointFloor;
			//new y = start_y + (slope * run_diff)
			var newY = data[startPointFloor].y + (movement * slope);

			return {x: createDateString(point), y:newY, rawVal: point}
		}

		var thisPlayerRange = playerRanges[i];
		var thisStartPoint = thisPlayerRange[0];
		var thisEndPoint = thisPlayerRange[1];
		var startData = createNewData(thisStartPoint);
		var endData = createNewData(thisEndPoint);
		if(startData){
			playerPoints.push(startData);
		}
		if(endData){
			playerPoints.push(endData);
		}
	}

	var totalData = data.concat(playerPoints);
	totalData.sort(function(a, b){ return a.rawVal - b.rawVal});

	return totalData;
}

function calculatePayouts(players, depositSums, totalDeposited, adjustedTotalDeposited, startingBr) {
	var backEndPointStartSize = 0.5;
	var frontEndStartSize = 0.5;
	var endingBr = totalDeposited;
	var frontWindowAdjustmentRatio = 1.4
	var backWindowAdjustmentRatio = 1.75;
	var frontWindowAdjustment = getEndPointSize(frontWindowAdjustmentRatio, startingBr, endingBr);
	var frontSectionSizeInPercent = frontEndStartSize/frontWindowAdjustment;
	var frontStartPoint = 1 - frontSectionSizeInPercent;
	var backWindowAdjustment = getEndPointSize(backWindowAdjustmentRatio, startingBr, endingBr);
	var backEndPoint = backEndPointStartSize/backWindowAdjustment;

	var frontSectionSize = pi/frontWindowAdjustment;
	var backSectionSize = pi/backWindowAdjustment;

	var payouts = [];
	for(var i=0; i<players.length; i++){
		var player = players[i];
		var frontPercent;
		if(i == 0) {
			frontPercent = 0;
		} else {
			var lastDepositSum = depositSums[i-1];
			frontPercent = lastDepositSum/totalDeposited;
		}

		var thisDepositSum = depositSums[i];
		var backPercent = thisDepositSum/totalDeposited;
		var winnings = 0;
		var startIntegrationPoint;
		var endIntegrationPoint;
		var mainAdjustmentRatio;

		function calcWinnings(fPercent, bPercent, sectionPercent, size, mainAdjustmentRatio, startAdjustment) {
			var startIntegrationPoint = startAdjustment + ((fPercent/sectionPercent) * size);
			var endIntegrationPoint = startAdjustment + ((bPercent/sectionPercent) * size);

			var int1 = integrate(endIntegrationPoint, mainAdjustmentRatio);
			var int2 = integrate(startIntegrationPoint, mainAdjustmentRatio)

			return ((int1 - int2) * adjustedTotalDeposited * mainAdjustmentRatio)/_2pi;
		}

		if(frontPercent < backEndPoint) {
			if(backPercent <= backEndPoint) {
				winnings += calcWinnings(frontPercent, backPercent, backEndPoint, backSectionSize, backWindowAdjustment, 0);
			} else if(backPercent <= frontStartPoint) {
				backPercent = backEndPoint;
				winnings += calcWinnings(frontPercent, backPercent, backEndPoint, backSectionSize, backWindowAdjustment, 0);
			} else {
				winnings += calcWinnings(frontPercent, backEndPoint, backEndPoint, backSectionSize, backWindowAdjustment, 0);
				winnings += calcWinnings(0, backPercent - frontStartPoint, frontSectionSizeInPercent, frontSectionSize, frontWindowAdjustment, frontSectionSize);
			}
		} else if(frontPercent < frontStartPoint) {
			if(backPercent <= frontStartPoint) {
				payouts.push(winnings);
				continue;
			} else {
				winnings += calcWinnings(0, backPercent - frontStartPoint, frontSectionSizeInPercent, frontSectionSize, frontWindowAdjustment, frontSectionSize);
			}
		} else {
			winnings += calcWinnings(frontPercent - frontStartPoint, backPercent - frontStartPoint, frontSectionSizeInPercent, frontSectionSize, frontWindowAdjustment, frontSectionSize);
		}
		payouts.push(winnings);
	}
	return payouts;
}